package main;
import java.io.*;

import menu.*;

public class Main {

	public static void main(String[] args) throws FileNotFoundException {
		MenuView menu = new MenuView();
		try {
			menu.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
