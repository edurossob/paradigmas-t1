package menu;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import historico.*;
import solicitacao.*;
import disciplina.*;
import aluno.*;

public class MenuView {

    private Scanner scanner = new Scanner(System.in);

    public void start() throws IOException{
        
		DisciplinaDAO.read_disciplinas();
        showInterface();
    }

    private void showInterface() throws IOException{
        
        System.out.println("Quebra de Barreira");

        int input = 0;

        boolean continua = true;

        do {
            System.out.println("----------------------");
            System.out.println("(1) Carrega histórico de aluno");
            System.out.println("(2) Mostra histórico");
            System.out.println("(3) Mostra matérias faltantes antes da barreira");
            System.out.println("(4) Mostra info último período");
            System.out.println("(5) Mostra disciplinas ainda não cursadas");
            System.out.println("(6) Realizar um pedido de quebra");
            System.out.println("(7) Fechar sistema");
            System.out.println("----------------------");
            System.out.print("Favor insira um número:");

            input = this.scanner.nextInt();

            switch (input) {
                case 1:
                    System.out.print("Carregando historico ...");
                    HistoricoDAO.read_historico(); 
                    System.out.println(" Pronto!");
                    break;
                case 2:
                    showHistorico();
                    break;
                case 3:
                    showMateriasAntesDaBarreira();
                    break;
                case 4:
                    showLastPeriodInfo();
                    break;
                case 5:
                    showMateriasNaoCursadas();
                    break;
                case 6:
                    System.out.println("\nInforme de qual grade você deseja realizar o pedido (2011 ou 2019)");
                    criaPedidoQuebra(this.scanner.nextInt());
                    break;
                case 7:
                    System.out.println("Até mais!");
                    continua = false;
                    break;
                default:
                    System.out.println("Digite um número válido");
                    break;
            }

        } while (continua);


        this.scanner.close();
        System.exit(1);
    }

    private void showHistorico(){
    	if(AlunoController.getCurrentAluno() == null) {
    		System.out.println("Carregue um aluno");
    		
    	}else {
    		HistoricoView.desenha(AlunoController.getCurrentAluno().getHistorico());
        }
    }

    private void showMateriasAntesDaBarreira(){
    	if(AlunoController.getCurrentAluno() == null) {
    		System.out.println("Carregue um aluno");
    		
    	}else {
    		HistoricoView.show_materias_faltantes_barreira(AlunoController.getCurrentAluno().getHistorico());
    	}
    }
    
    private void showLastPeriodInfo() {
    	if(AlunoController.getCurrentAluno() == null) {
    		System.out.println("Carregue um aluno");
    		
    	}else {
    		HistoricoView.show_last_period_info(AlunoController.getCurrentAluno().getHistorico());
    	}
    }

    private void showMateriasNaoCursadas() {
        Aluno aluno = AlunoController.getCurrentAluno();
        if(aluno == null){
            System.out.println("Carregue um aluno");
            return;
        }
        DisciplinaView.showMateriasNaoCursadas(aluno);   
    }

    private void criaPedidoQuebra(int ano) throws IOException {
        Aluno aluno = AlunoController.getCurrentAluno();
        if(aluno == null){
            System.out.println("Carregue um aluno");
            return;
        }
        List<Disciplina> disciplinasNaoCursadas = DisciplinaController.disciplinasNaoCursadasSorted(aluno, ano);
        DisciplinaView.showMateriasNaoCursadas(disciplinasNaoCursadas);

        System.out.println("Informe o código das matérias que desejar pedir a quebra.\nInsira '0' quando terminar");
        Solicitacao solicitacao = SolicitacaoController.realizaPedido(aluno, leCodigos());
        String caminho = SolicitacaoDAO.geraPedido(solicitacao);
        System.out.println("O seu pedido está salvo em " + caminho);
    }

    private List<String> leCodigos() {
		List<String> codigos = new ArrayList<>();
        this.scanner.nextLine(); // le quebra de linha
        String temp = this.scanner.nextLine();

        while(!temp.equals("0")) {
            codigos.add(temp);
            temp = this.scanner.nextLine();
        }

		return codigos;
	}
}