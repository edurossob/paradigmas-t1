package solicitacao;

import java.io.FileWriter;
import java.io.IOException;

import disciplina.Disciplina;

public class SolicitacaoDAO {
    public static String geraPedido(Solicitacao solicitacao) throws IOException {
        try {
            String filePath = "/home/erick-graeff/University/paradinhas/trabalho/projeto-git/paradigmas-t1/src/main/resources/pedido.txt";
            FileWriter fw = new FileWriter(filePath);

            fw.write("Nome: "+ solicitacao.getAluno().getNome() + "\n");
            fw.write("GRR: "+ solicitacao.getAluno().getMatricula() + "\n");
            fw.write("Disciplinas:\n");
			for (Disciplina disciplina : solicitacao.getDisciplinasSolicitadas()) {
				fw.write("\t"+disciplina+"\n");
			}

            fw.close();
			return filePath;
        }
        catch (Exception e) {
            e.getStackTrace();
        }

		return "";
    }
}
