package solicitacao;

import java.util.List;

import aluno.Aluno;
import disciplina.Disciplina;

public class Solicitacao {
	private Aluno aluno;
	private List<Disciplina> disciplinasSolicitadas;

	public Solicitacao(Aluno aluno, List<Disciplina> disciplinasSolicitadas) {
		this.aluno = aluno;
		this.disciplinasSolicitadas = disciplinasSolicitadas;
	}

	public Aluno getAluno() {
		return this.aluno;
	}

	public List<Disciplina> getDisciplinasSolicitadas() {
		return this.disciplinasSolicitadas;
	}
}
