package solicitacao;

import java.util.ArrayList;
import java.util.List;

import aluno.Aluno;
import disciplina.*;

public class SolicitacaoController {
	public static Solicitacao realizaPedido(Aluno aluno, List<String> codigosPedido) {
		System.out.println(">>>>>PRINTANDO OS CODIGOS");
        for(String c : codigosPedido) {
            System.out.println(c);
        }

		List<Disciplina> disciplinasSolicitadas = new ArrayList<>();

		for (String codigo : codigosPedido) {
			if(DisciplinaDAO.findDisciplina(codigo) == null) {
				System.out.println("Não foi possível encontrar uma disciplina com o código [" + codigo + "]");
			}
			disciplinasSolicitadas.add(DisciplinaDAO.findDisciplina(codigo));
		}

		return new Solicitacao(aluno, disciplinasSolicitadas);
	}
}
