package utils;

import java.util.Comparator;

import disciplina.Disciplina;

public class SortByPeriod implements Comparator<Disciplina> {

    @Override
    public int compare(Disciplina arg0, Disciplina arg1) {
        if(arg0.getPeriodo_ideal() < arg1.getPeriodo_ideal()) return -1;
        if(arg0.getPeriodo_ideal() > arg1.getPeriodo_ideal()) return 1;
        return 0;
    }
}