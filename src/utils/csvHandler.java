package utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class csvHandler {
	
	public static List<List<String>> readCSV(String file_dir) {
		
		
		File file = new File (file_dir);
		
		
		List<List<String>> list = new ArrayList<>();

		
		try {
		    BufferedReader br = new BufferedReader(new FileReader(file));


		    String line = br.readLine();
		    line = br.readLine();
		    String[] tempArr;
		    
			while ((line = br.readLine()) != null) {
				tempArr = line.split(";");

				list.add(Arrays.asList(tempArr));
			}
			
	        br.close();

		} catch (Exception e) { e.printStackTrace(); }


		return list;
	}



}
