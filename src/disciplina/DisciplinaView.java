package disciplina;

import java.util.List;

import aluno.Aluno;
import view.Display;

public class DisciplinaView {
    public static void showMateriasNaoCursadas(Aluno aluno) {
        List<Disciplina> materiasNaoCursadas = DisciplinaController.disciplinasNaoCursadasSorted(aluno);
        int size = materiasNaoCursadas.size();

        String[][] data = new String[size][3];

        for(int i = 0; i < size; i++) {
            Disciplina currentDisciplina = materiasNaoCursadas.get(i);
            String[] line = {currentDisciplina.getCod(), currentDisciplina.getNome(), String.valueOf(currentDisciplina.getPeriodo_ideal())};

            data[i] = line;
        }

        String column[] = {"Código da disciplina", "Nome disciplina", "Período ideal para cursar"};
        String name = "Matérias não cursadas";

        new Display(name, data, column);
    }

    public static void showMateriasNaoCursadas(List<Disciplina> materiasNaoCursadas) {
        int size = materiasNaoCursadas.size();

        String[][] data = new String[size][3];

        for(int i = 0; i < size; i++) {
            Disciplina currentDisciplina = materiasNaoCursadas.get(i);
            String[] line = {currentDisciplina.getCod(), currentDisciplina.getNome(), String.valueOf(currentDisciplina.getPeriodo_ideal())};

            data[i] = line;
        }

        String column[] = {"Código da disciplina", "Nome disciplina", "Período ideal para cursar"};
        String name = "Matérias não cursadas";

        new Display(name, data, column);
    }
}
