package disciplina;

import curso.Curso;

public class Disciplina {
	Curso curso;
	private String cod;	// ci055
	private int num_versao; // 2011
	private String nome; // ALGORITMOS E ESTRUTURAS DE DADOS I
	private int periodo_ideal; // 1
	private int num_horas; // 30
	private int ch_total; // 60
	private String tipo; // obrigatória
	private String situacao_versao; // A	|| Apenas 2011
	private String descr_estrutura; // Núcleo de Conte[udos Obrigatórios	|| Apenas 2019

	public Disciplina(Curso curso, String cod, int num_versao, String nome, int periodo_ideal, int num_horas,
			int ch_total, String tipo, String situacao_versao, String descr_estrutura) {
		super();
		this.curso = curso;
		this.cod = cod;
		this.num_versao = num_versao;
		this.nome = nome;
		this.periodo_ideal = periodo_ideal;
		this.num_horas = num_horas;
		this.ch_total = ch_total;
		this.tipo = tipo;
		this.situacao_versao = situacao_versao;
		this.descr_estrutura = descr_estrutura;
	}
	
	public Curso getCurso() {
		return curso;
	}
	public void setCurso(Curso curso) {
		this.curso = curso;
	}
	public String getCod() {
		return cod;
	}
	public void setCod(String cod) {
		this.cod = cod;
	}
	public int getNum_versao() {
		return num_versao;
	}
	public void setNum_versao(int num_versao) {
		this.num_versao = num_versao;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getPeriodo_ideal() {
		return periodo_ideal;
	}
	public void setPeriodo_ideal(int periodo_ideal) {
		this.periodo_ideal = periodo_ideal;
	}
	public int getNum_horas() {
		return num_horas;
	}
	public void setNum_horas(int num_horas) {
		this.num_horas = num_horas;
	}
	public int getCh_total() {
		return ch_total;
	}
	public void setCh_total(int ch_total) {
		this.ch_total = ch_total;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getSituacao_versao() {
		return situacao_versao;
	}
	public void setSituacao_versao(String situacao_versao) {
		this.situacao_versao = situacao_versao;
	}
	public String getDescr_estrutura() {
		return descr_estrutura;
	}
	public void setDescr_estrutura(String descr_estrutura) {
		this.descr_estrutura = descr_estrutura;
	}
	
	public String toString() {
		String a = "";
		
		String separator = "\n";
		
		a += "cod: " + this.cod + separator;
		a += "   : " + this.curso.getNome() + separator;
		a += "   : " + this.cod + separator;
		a += "   : " + this.num_versao + separator;
		a += "   : " + this.nome + separator;
		a += "   : " + this.periodo_ideal + separator;
		a += "   : " + this.num_horas + separator;
		a += "   : " + this.ch_total + separator;
		a += "   : " + this.tipo + separator;
		a += "   : " + this.situacao_versao + separator;
		a += "   : " + this.descr_estrutura + separator;
		
		return a;
	}

}
