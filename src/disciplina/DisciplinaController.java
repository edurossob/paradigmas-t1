package disciplina;

import java.util.Collections;
import java.util.List;

import aluno.Aluno;
import utils.SortByPeriod;

public class DisciplinaController {

    public static List<Disciplina> disciplinasNaoCursadasSorted(Aluno aluno) {
        List<Disciplina> listDisciplina = DisciplinaDAO.getDisciplinasNaoCursadas(aluno);
        Collections.sort(listDisciplina, new SortByPeriod());

        return listDisciplina;
    }

    public static List<Disciplina> disciplinasNaoCursadasSorted(Aluno aluno, int ano) {
        List<Disciplina> listDisciplina = DisciplinaDAO.getDisciplinasNaoCursadas(aluno, ano);
        Collections.sort(listDisciplina, new SortByPeriod());

        return listDisciplina;
    }
}
