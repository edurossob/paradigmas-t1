package disciplina;

import java.util.ArrayList;
import java.util.List;

import aluno.Aluno;
import curso.Curso;
import curso.CursoController;
import historico.Historico;
import utils.csvHandler;

public class DisciplinaDAO {

    private static List<Disciplina> disciplinas;

    /* 2019 header
	 * COD_CURSO;NUM_VERSAO;DESCR_ESTRUTURA;COD_DISCIPLINA;NOME_UNIDADE;NOME_DISCIPLINA;
		PERIODO_IDEAL;NUM_HORAS;TIPO_DISCIPLINA;
		CH_TOTAL;DESCR_SITUACAO
	 
	 * 2011 header
	 * COD_CURSO;NUM_VERSAO;DESCR_ESTRUTURA;COD_DISCIPLINA;NOME_UNIDADE;NOME_DISCIPLINA;
	 * PERIODO_IDEAL;NUM_HORAS;TIPO_DISCIPLINA;
	 * SITUACAO_VERSAO;CH_TOTAL
	*/
	
	// defines para o csv
	private static final int COD_CURSO		= 0;	// curso info
	private static final int NUM_VERSAO		= 1;
	private static final int DESC_ESTRUTURA	= 2;
	private static final int COD_DISC		= 3;
	private static final int NOME_UNIDADE	= 4;	// CURSO INFO
	private static final int NOME_DISC 		= 5;
	private static final int PERIODO_IDEAL 	= 6;
	private static final int NUM_HORAS 		= 7;
	private static final int TIPO 			= 8;
	private static final int CH_TOTAL_2019	= 9;
	private static final int SITUACAO_2019	= 10;
	private static final int CH_TOTAL_2011	= 10;
	private static final int SITUACAO_2011	= 9;

    public static List<Disciplina> getDisciplinas() {
		return disciplinas;
	}

    public static List<Disciplina> read_disciplinas(){
		disciplinas = new ArrayList<>();

		String file_dir_2019 = "/home/erick-graeff/University/paradinhas/trabalho/projeto-git/paradigmas-t1/src/main/resources/disciplinas2019.csv";
		String file_dir_2011 = "/home/erick-graeff/University/paradinhas/trabalho/projeto-git/paradigmas-t1/src/main/resources/disciplinas2011.csv";

		// String file_dir_2019 = "/home/eduardo-barbosa/BCC/paradigmas-t1/src/main/resources/disciplinas2019.csv";
		// String file_dir_2011 = "/home/eduardo-barbosa/BCC/paradigmas-t1/src/main/resources/disciplinas2011.csv";


		input_disc(file_dir_2011);
		input_disc(file_dir_2019);

		return disciplinas;
	}

	private static void input_disc(String file_dir){

		List<List<String>> list = new ArrayList<>();
		list = csvHandler.readCSV(file_dir);

		for(List<String> d : list) {

			String cod = d.get(COD_DISC);
			String nome = d.get(NOME_DISC);
			int versao = Integer.parseInt(d.get(NUM_VERSAO));
			String cod_curso = d.get(COD_CURSO);
			String nome_unidade = d.get(NOME_UNIDADE);

			Curso curso = CursoController.findCurso(cod_curso, nome_unidade);

            int periodo = 0;
            String tipo = " ";
            int num_horas = 0;
            String descr_estrutura = "";

            int ch_total = 0;
            String situacao = "";

            if(d.size() > 6) {
                String tmp = d.get(PERIODO_IDEAL);
                if(tmp.length() > 0) periodo = Integer.parseInt(tmp);
                
	            tipo = d.get(TIPO);
	            num_horas = Integer.parseInt(d.get(NUM_HORAS));
	            descr_estrutura = d.get(DESC_ESTRUTURA);

	            if(versao == 2019) {
	                ch_total = Integer.parseInt(d.get(CH_TOTAL_2019));
	                situacao = d.get(SITUACAO_2019);
	            } else {
	                ch_total = Integer.parseInt(d.get(CH_TOTAL_2011));
	                situacao = d.get(SITUACAO_2011);
	            }
            }

			Disciplina disciplina = findDisciplina(cod);
			if(disciplina != null) {
				disciplina.setCh_total(disciplina.getCh_total() + ch_total);

				continue;
			}

            disciplina = new Disciplina (
            	curso, 
				cod,
				versao,
				nome,
				periodo,
				num_horas, 
				ch_total, 
				tipo, 
				situacao, 
				descr_estrutura
			);
            
            
            disciplinas.add(disciplina);
		}
			
	}

	public static List<Disciplina> disciplinasObrigatorias() {
		List<Disciplina> disciplinasObrigatorias = new ArrayList<>();

		for (Disciplina disciplina : disciplinas) {
			if(disciplina.getPeriodo_ideal() != 0) disciplinasObrigatorias.add(disciplina);
		}

		return disciplinasObrigatorias;
	}

	public static List<Disciplina> disciplinasObrigatorias(int ano) {
		List<Disciplina> disciplinasObrigatorias = new ArrayList<>();

		for (Disciplina disciplina : disciplinas) {
			if(disciplina.getPeriodo_ideal() != 0 && disciplina.getNum_versao() == ano) disciplinasObrigatorias.add(disciplina);
		}

		return disciplinasObrigatorias;
	}

    public static Disciplina findDisciplina(String codigo) {
		for(Disciplina d : disciplinas) {
			if(d.getCod().equals(codigo)) return d;
		}

		return null;
	}

    public static List<Disciplina> materias_na_barreira() {
		List<Disciplina> cod_disciplinas_barreira = new ArrayList<>();

		for (Disciplina disciplina : disciplinasObrigatorias()) {
			if(disciplina.getPeriodo_ideal() < 4 && disciplina.getNum_versao() < 2019 ){
				cod_disciplinas_barreira.add(disciplina);
			}
		}

		return cod_disciplinas_barreira;
	}

	public static List<Disciplina> getDisciplinasNaoCursadas(Aluno aluno) {
		List<Disciplina> listDisciplina = disciplinasObrigatorias();
    	
    	for(Historico h : aluno.getHistorico()) {
    		if(h.getSituacao_item() == 1) listDisciplina.remove(h.getDisciplina());
    	}
    	
    	return listDisciplina;
	}

	public static List<Disciplina> getDisciplinasNaoCursadas(Aluno aluno, int ano) {
		List<Disciplina> listDisciplina = disciplinasObrigatorias(ano);
    	
    	for(Historico h : aluno.getHistorico()) {
    		if(h.getSituacao_item() == 1) listDisciplina.remove(h.getDisciplina());
    	}
    	
    	return listDisciplina;
	}
}
