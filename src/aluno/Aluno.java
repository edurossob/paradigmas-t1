package aluno;

import java.util.List;
import java.util.ArrayList;

import historico.Historico;
import curso.Curso;

public class Aluno {
	private String Matricula;	// grr
	private String Nome;	// nome
	private double ira;		// ira
	private Curso curso;	// Curso
	private List<Historico> hist;	// Historico
	
	public Aluno(String matricula, String nome, Curso curso) {
		super();
		this.Matricula = matricula;
		this.Nome = nome;
		this.ira = -1;
		this.curso = curso;
		this.hist = new ArrayList<>();
	}

	public List<Historico> getHistorico() {
		return hist;
	}

    public void addHistorico(Historico h){
        hist.add(h);
    }

	public String getMatricula() {
		return Matricula;
	}

	public void setMatricula(String matricula) {
		Matricula = matricula;
	}

	public String getNome() {
		return Nome;
	}

	public void setNome(String nome) {
		Nome = nome;
	}

	public double getIra() {
		return ira;
	}

	public void setIra(double ira) {
		this.ira = ira;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}	
}
