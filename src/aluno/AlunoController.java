package aluno;

import java.util.ArrayList;
import java.util.List;

import curso.CursoController;

public class AlunoController {
    private static List<Aluno> alunos;
    private static Aluno currentAluno;

    public static Aluno getCurrentAluno(){
        return currentAluno;
    }

    public static Aluno findAluno(String grr, String nome, String cod_curso, String nome_curso) {
        if(alunos == null) alunos = new ArrayList<>();

        for(Aluno a : alunos) {
            if(a.getMatricula().equals(grr)) return a;
        }

        return createAluno(grr, nome, cod_curso, nome_curso);
    }

    private static Aluno createAluno(String grr, String nome, String cod_curso, String nome_curso) {
        Aluno aluno = new Aluno(grr,
         nome, 
         CursoController.findCurso(cod_curso, nome_curso));

        alunos.add(aluno);
        currentAluno = aluno;

        return aluno;
    }

    public static List<Aluno> getAlunos() {
        return alunos;
    }
}
