package view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;


import javax.swing.JScrollPane;

import java.awt.BorderLayout;

public class Display {
    public Display(String name, String[][] data, String[] column) {
        JFrame frame = new JFrame(name);

        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());    
                          
        JTable table = new JTable(data, column);

        JScrollPane tableContainer = new JScrollPane(table);

        panel.add(tableContainer, BorderLayout.CENTER);
        frame.getContentPane().add(panel);

        frame.pack();
        frame.setVisible(true);
    }
}