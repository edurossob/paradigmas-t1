package curso;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CursoController {
	
	private static List<Curso> cursos;

	public static Curso create(String cod_curso, String nome) {

		if(cursos == null) { cursos = new ArrayList<>(); }
		
		Curso curso = new Curso(cod_curso, nome);
		cursos.add(curso);

		return curso;
	}
	
	public static Curso findCurso(String cod_curso, String nome) {

		if(cursos == null) { cursos = new ArrayList<>();}

		
	    Iterator<Curso> iterator = cursos.iterator();
	    Curso curso;
	    while (iterator.hasNext()) {
	        curso = iterator.next();
	        if (curso.getCod().equals(cod_curso)) {
	            return curso;
	        }
	    }
	    return create(cod_curso, nome);
		

	}
}
