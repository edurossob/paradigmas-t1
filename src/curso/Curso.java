package curso;

public class Curso {
	private String cod; // 21A
	private String nome; // Curso de Ciência da Computação - Bacharelado

	public Curso(String cod, String nome) {
		this.cod = cod;
		this.nome = nome;
	}
	
	public String getCod() {
		return cod;
	}
	public void setCod(String cod) {
		this.cod = cod;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}
