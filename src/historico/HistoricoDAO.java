package historico;

import java.util.*;

import disciplina.*;
import aluno.*;
import utils.csvHandler;

public class HistoricoDAO {
    // defines para o csv
	private static final int MATR_ALUNO	        = 0;	
	private static final int NOME_PESSOA	    = 1;
	private static final int COD_CURSO	        = 2;
	private static final int NOME_CURSO		    = 3;
	// private static final int NUM_VERSAO	    = 4;	// disciplina info
	private static final int ANO                = 5;
	private static final int MEDIA_FINAL 	    = 6;
	private static final int SITUACAO_ITEM 		= 7;
	private static final int PERIODO 			= 8;
	private static final int SITUACAO	        = 9;
	private static final int COD_ATIV_CURRIC	= 10;
	// private static final int NOME_ATIV_CURRIC= 11;   // disciplina info
	// private static final int CH_TOTAL	    = 12;   // disciplina info
	// private static final int DESCR_ESTRUTURA	= 13;   // disciplina info
	private static final int FREQUENCIA	        = 14;
	private static final int SIGLA              = 15;

	public static void read_historico(){
        
        String file_dir = "/home/erick-graeff/University/paradinhas/trabalho/projeto-git/paradigmas-t1/src/main/resources/historico.csv";

		// String file_dir = "/home/eduardo-barbosa/BCC/paradigmas-t1/src/main/resources/historico.csv";
		
		List<List<String>> list = new ArrayList<>();
		list = csvHandler.readCSV(file_dir);

        String matr = list.get(0).get(MATR_ALUNO);
        String nome_aluno = list.get(0).get(NOME_PESSOA);
        String cod_curso = list.get(0).get(COD_CURSO);
        String nome_curso = list.get(0).get(NOME_CURSO);

        Aluno aluno = AlunoController.findAluno(matr, nome_aluno, cod_curso, nome_curso);
        int anoInicial = Integer.parseInt(list.get(0).get(ANO));

		for(List<String> d : list) {

            String cod_materia = d.get(COD_ATIV_CURRIC);

            Disciplina disciplina = DisciplinaDAO.findDisciplina(cod_materia);
            if(disciplina == null) continue;

            int ano = Integer.parseInt( d.get(ANO) );

            int media_final = Integer.parseInt( d.get(MEDIA_FINAL) );
            int situacao_item = Integer.parseInt( d.get(SITUACAO_ITEM) );
            String periodo = d.get(PERIODO);
            String situacao = d.get(SITUACAO);

            String tmp = d.get(FREQUENCIA);
            int frequencia = (tmp.length() > 0) ? Integer.parseInt( tmp ) : 0;
            String sigla = d.get(SIGLA);

            int periodo_cursado = 2*(ano - anoInicial) + periodo.charAt(0) - '0'; /* usando artificios tecnicos */

            Historico h = new Historico(
                aluno,
                disciplina,
                ano,
                media_final,
                situacao_item, 
                periodo,
                situacao,
                frequencia,
                sigla,
                periodo_cursado
            );


            aluno.addHistorico(h);
        }
    }

    public static List<Disciplina> materias_faltantes_barreira(List<Historico> list) {
        List<Disciplina> listDisciplina = DisciplinaDAO.materias_na_barreira();
        List<Disciplina> disciplinas_aprovado = new ArrayList<>();

        for(Historico historico : list) {
            if(!listDisciplina.contains(historico.getDisciplina())) continue;

            if(historico.getSituacao_item() == 1) {
                disciplinas_aprovado.add(historico.getDisciplina());
                continue;
            }
        }

        for (Disciplina disciplina : disciplinas_aprovado) {

            if(listDisciplina.contains(disciplina))
                listDisciplina.remove(disciplina);
        }

        return listDisciplina;
    }
    
    public static List<Disciplina> materiasFaltantes(List<Historico> list) {
    	List<Disciplina> listDisciplina = DisciplinaDAO.getDisciplinas();
    	
    	for(Historico h : list) {
    		if(h.getSituacao_item() == 1) listDisciplina.remove(h.getDisciplina());
    	}
    	
    	return listDisciplina;
    }
}
