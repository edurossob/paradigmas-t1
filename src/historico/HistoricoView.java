package historico;

import java.util.List;

import view.Display;
import disciplina.*;

public class HistoricoView {
    

    public static void desenha (List<Historico> list) {

        String [][] data = new String[list.size()][3];

        for (int i = 0; i < list.size(); i++) {

            String[] line_value = {String.valueOf(list.get(i).getPeriodo_cursado()), list.get(i).getDisciplina().getCod(), list.get(i).getDisciplina().getNome(), list.get(i).getSituacao()};

            data[i] = line_value;
        }

        String column[]={"Periodo em que cursou","Código matéria","Nome matéria", "Situação matéria"};    

        String name = "Historico";

        new Display(name, data, column);
    }

    public static void show_materias_faltantes_barreira(List<Historico> list) {
        List<Disciplina> materias = HistoricoDAO.materias_faltantes_barreira(list);

        String[][] data = new String[materias.size()][4];

        for(int i = 0; i < materias.size(); i++) {
            String[] line_value = {materias.get(i).getNome(), materias.get(i).getCod(), String.valueOf(materias.get(i).getPeriodo_ideal())};
            data[i] = line_value;
        }

        String column[] = {"Nome disciplina", "Código da disciplina", "Período ideal para cursar"};
        String name = "Matérias que faltam para a barreira";

        new Display(name, data, column);
    }
    
	public static void show_last_period_info(List<Historico> list){
		int lastPeriod= 0;
		int lastLastPeriodo = 0;
		
		for(Historico h : list) {
			if(h.getPeriodo_cursado() > lastPeriod) {
				lastLastPeriodo = lastPeriod;
				lastPeriod = h.getPeriodo_cursado();
			}
		}

		int total = 0;
		int aprovacoes = 0;
		int reprovado_falta = 0;

		System.out.println("Ultimo periodo "+ lastLastPeriodo);
		for(Historico h : list) {
			if(h.getPeriodo_cursado() == lastLastPeriodo) {
				total+=1;
				
				if(h.getSituacao().equals("Aprovado"))	// caso aprovado
					aprovacoes+=1;
				
				if(h.getSituacao().equals("Reprovado por nota"))	// caso aprovado
					reprovado_falta +=1;

			}
		}
		
		int porcentagem = 0;
		if(total > 0) {
			porcentagem = ((aprovacoes*100 / total) );
		}
		System.out.println("\n>  Aprovado em " + porcentagem
						+ "%\n>  Reprovado por falta em " + reprovado_falta + "\n");
		
	}
}