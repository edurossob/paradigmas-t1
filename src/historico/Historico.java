package historico;

import disciplina.*;
import aluno.*;

public class Historico {
	/* MATR_ALUNO	NOME_PESSOA	COD_CURSO	NOME_CURSO	NUM_VERSAO	ANO	MEDIA_FINAL
 	 * SITUACAO_ITEM	PERIODO	SITUACAO	COD_ATIV_CURRIC	NOME_ATIV_CURRIC	CH_TOTAL	DESCR_ESTRUTURA	FREQUENCIA	SIGLA
 	*/
    private Aluno aluno;
	private Disciplina disciplina;
    private int ano;
    private int media_final;
    private int situacao_item;
    private String periodo;
    private String situacao;
    private int frequencia;
    private String sigla;
    private int periodo_cursado;

    public Historico( Aluno aluno, Disciplina disciplina, int ano, int media_final, int situacao_item, String periodo, String situacao, int frequencia, String sigla, int periodo_cursado) {
        this.disciplina = disciplina;
        this.aluno = aluno;
		this.ano = ano;
		this.media_final = media_final;
		this.situacao_item = situacao_item;
		this.periodo = periodo;
		this.situacao = situacao;
		this.frequencia = frequencia;
		this.sigla = sigla;
		this.periodo_cursado = periodo_cursado;
	}

    public int getPeriodo_cursado() {
		return periodo_cursado;
	}

	public void setPeriodo_cursado(int periodo_cursado) {
		this.periodo_cursado = periodo_cursado;
	}

	public Aluno getAluno() {
        return aluno;
    }

    public void setAluno(Aluno aluno) {
        this.aluno = aluno;
    }

	public Disciplina getDisciplina() {
		return disciplina;
	}
	public void setDisciplina(Disciplina disciplina) {
		this.disciplina = disciplina;
	}
	public int getAno() {
		return ano;
	}
	public void setAno(int ano) {
		this.ano = ano;
	}
	public int getMedia_final() {
		return media_final;
	}
	public void setMedia_final(int media_final) {
		this.media_final = media_final;
	}
	public int getSituacao_item() {
		return situacao_item;
	}
	public void setSituacao_item(int situacao_item) {
		this.situacao_item = situacao_item;
	}
	public String getPeriodo() {
		return periodo;
	}
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}
	public String getSituacao() {
		return situacao;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	public int getFrequencia() {
		return frequencia;
	}
	public void setFrequencia(int frequencia) {
		this.frequencia = frequencia;
	}
	public String getSigla() {
		return sigla;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
}
